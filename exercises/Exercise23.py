primes = []
happies = []
overLap = []

def readPrimes():
    with open('primes.txt') as primes_file:
        for line in primes_file:
            primes.append(int(line))

def readHappies():
    with open('happies.txt') as happies_file:
        for line in happies_file:
            happies.append(int(line))

readPrimes()
readHappies()

for num in primes:
    if num in happies:
        overLap.append(num)
		
print(overLap)