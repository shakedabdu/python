def print_board(game):
    for row in range(0, 3):
        if game[row][0] == 0:
            print('-', end =" ")
        elif game[row][0] == 1:
            print('X', end =" ")
        else:
            print('O', end =" ")
        if game[row][1] == 0:
            print('-', end =" ")
        elif game[row][1] == 1:
            print('X', end =" ")
        else:
            print('O', end =" ")
        if game[row][2] == 0:
            print('-')
        elif game[row][2] == 1:
            print('X')
        else:
            print('O')

board = [[0, 0, 0],
	[0, 0, 0],
	[0, 0, 0]]
player_turn = 'X'
player_cell = 1
game_status = 0 # 0 - active game | 1 - player 1 win | 2 - player 2 win

while game_status == 0:
	location = str(input('Player {} enter row,col : '.format(player_turn)))
	row = int(location[0]) - 1
	col = int(location[2]) - 1
	while board[row][col] != 0 :
		location = str(input('Error, player {} please enter other row,col : '.format(player_turn)))
		row = int(location[0]) - 1
		col = int(location[2]) - 1

	board[row][col] = player_cell

	if player_turn == 'X':
		player_turn = 'Y'
		player_cell = 2
	else :
		player_turn = 'X'
		player_cell = 1
	print_board(board)