import random
words = []

with open('sowpods.txt') as sowpods_file:
    for line in sowpods_file:
        words.append(str(line))

word = random.choice(words).strip()
guess = "_" * len(word)

word_list = list(word)
guess_list = list(guess)

guessed = []

letter = input('Guess a letter : ')

finish = False

while finish == False:
    if letter.upper() in guessed:
        letter = ''
        print('already guessed this letter before')
    elif letter.upper() in word_list:
        index = word_list.index(letter.upper())
        guess_list[index] = letter.upper()
        word_list[index] = '_'
    else:
        print(''.join(guess_list))
        if letter is not '':
            guessed.append(letter.upper())
        letter = input("guess letter: ")
    if '_' not in guess_list:
        finish = True

print("Good job !")