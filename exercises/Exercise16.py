import random
import string

password = ''

# password length = 15

for number in range(1,6):
    password += str(random.randint(0,9))
    password += random.choice(string.ascii_uppercase)
    password += random.choice(string.ascii_lowercase)

print(password)