import requests
from bs4 import BeautifulSoup

website_url = 'http://www.nytimes.com'
req = requests.get(website_url)
soup = BeautifulSoup(req.text, features="html.parser")

headers = soup.find_all('h2')


for element in headers:
    print(element.text)