num = int(raw_input("Enter number: "))
isPrime = 1
for x in range(1, num):
    if num % x == 0:
        isPrime = x

if isPrime == 1:
    print('{} is prime'.format(num))
else:
    print('{} is not prime'.format(num))