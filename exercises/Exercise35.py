from collections import Counter

months = {
	1: "January",
	2: "February",
	3: "March", 
	4: "April",
	5: "May",
	6: "June",
	7: "July",
	8: "August",
	9: "September",
	10: "October",
	11: "November",
	12: "December"
}

birthdays = {
    'Eli Ozeri': '03/14/1879',
    'Benjamin Enil': '01/17/1706',
    'Adar Machiah': '12/10/1815',
    'Donald Trump': '06/14/1946',
    'Denver Atkinson': '01/6/1955'}

month_counter = []
for name, birthday in birthdays.items():
    month = int(birthday.split("/")[0])
    month_counter.append(months[month])

print(Counter(month_counter))