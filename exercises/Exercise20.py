from bisect import bisect_left 


def isExist(a, num):
    i = bisect_left(a, num) 
    if i != len(a) and a[i] == num: 
        return i 
    else: 
        return -1
        
a = [1, 1, 3, 6, 6, 6, 7, 7, 8, 10, 12, 33, 122, 155]
num = 12
if isExist(a, num) == -1:
    print('not fount')
else:
    print('found')