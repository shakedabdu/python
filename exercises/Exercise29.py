def check_rows(game):
    for i in range(0, 3):
        if (game[i][0] == game[i][1] == game[i][2]) and (game[i][2] is not 0):
            return game[i][0]
    return 0
		
def check_cols(game):
    for i in range(0,3):
    	if (game[0][i] == game[1][i] == game[2][i]) and (game[2][i] is not 0):
    		return game[0][i]
    return 0

def check_diag(game):
    if (game[0][0] == game[1][1] == game[2][2]) and (game[0][0] is not 0):
        return game[0][0]
    if (game[0][2] == game[1][1] == game[2][0]) and (game[0][2] is not 0):
        return game[0][2]
    return 0

def check_game(game):
	if check_rows(game) == 1 or check_diag(game) == 1 or check_cols(game) == 1:
		return 1
	elif check_rows(game) == 2 or check_diag(game) == 2 or check_cols(game) == 2:
		return 2
	return 0

def print_board(game):
    for row in range(0, 3):
        if game[row][0] == 0:
            print('-', end =" ")
        elif game[row][0] == 1:
            print('X', end =" ")
        else:
            print('O', end =" ")
        if game[row][1] == 0:
            print('-', end =" ")
        elif game[row][1] == 1:
            print('X', end =" ")
        else:
            print('O', end =" ")
        if game[row][2] == 0:
            print('-')
        elif game[row][2] == 1:
            print('X')
        else:
            print('O')

board = [[0, 0, 0],
	[0, 0, 0],
	[0, 0, 0]]

player_turn = 'X'
player_cell = 1
game_status = 0 # 0 - active game | 1 - player 1 win | 2 - player 2 win


while game_status == 0:
    location = str(input('Player {} enter row,col : '.format(player_turn)))
    row = int(location[0]) - 1
    col = int(location[2]) - 1
    while board[row][col] != 0 :
        location = str(input('Error, player {} please enter other row,col : '.format(player_turn)))
        row = int(location[0]) - 1
        col = int(location[2]) - 1

    board[row][col] = player_cell

    if player_turn == 'X':
        player_turn = 'Y'
        player_cell = 2
    else :
        player_turn = 'X'
        player_cell = 1
    game_status = check_game(board)
    print_board(board)

if game_status == 1:
    print('player X win')
else:
    print('player Y win')