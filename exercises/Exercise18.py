import random

final_len = 4 

def bulls(number, guess):
    str_num = str(number)
    str_guess = str(guess)
    counter = 0
    for digit in str_num:
        if digit in str_guess:
            counter += 1
    return counter

def cows(number, guess):
    str_num = str(number)
    str_guess = str(guess)
    counter = 0
    for index in range(0, final_len):
        if str_guess[index] == str_num[index]:
            counter += 1
    return counter

def game():
    number = random.randint(1000, 9999)

    user_guess = int(input("guess number 1000 - 9999 : "))

    while user_guess != number:
        print("cows {cows} , bulls {bulls}".format(cows = cows(number, user_guess), bulls = bulls(number, user_guess)))
        user_guess = int(input("guess number 1000 - 9999 : "))
    print('Good guess !!!')

game()