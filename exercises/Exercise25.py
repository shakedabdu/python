guess_counter = 1
number = 50
print('Think abuot number 0-100 ... ')
user_answer = str(input('the number is {} ? ( yes / lower / higher ) : '.format(number)))

while user_answer != 'yes':
    guess_counter += 1
    if number == 50:
        if user_answer == 'lower':
            number /=2
        else:
            number *= 1.5
    elif  number == 75:
        if user_answer == 'lower':
            number = 62.5
        else:
            number = 87.5
    elif  number == 25:
        if user_answer == 'lower':
            number = 12.5
        else:
            number = 37.5
    else:
        if user_answer == 'lower':
            number -= 1
        else:
            number += 1
    number = int(number)
    user_answer = str(input('the number is {} ? ( yes / lower / higher ) : '.format(number)))

print('It took {} guesses !'.format(guess_counter))