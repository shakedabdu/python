winner_is_2 = [[2, 2, 0],
	[2, 1, 0],
	[2, 1, 1]]

winner_is_1 = [[1, 2, 0],
	[2, 1, 0],
	[2, 1, 1]]

winner_is_also_1 = [[0, 1, 0],
	[2, 1, 0],
	[2, 1, 1]]

no_winner = [[1, 2, 0],
	[2, 1, 0],
	[2, 1, 2]]

also_no_winner = [[1, 2, 0],
	[2, 1, 0],
	[2, 1, 0]]

def check_rows(game):
	for i in range(0, 3):
		if (game[i][0] == game[i][1] == game[i][2]) and (game[i][2] is not 0):
			return game[i][0]
	return 0
		
def check_cols(game):
    for i in range(0,3):
    	if (game[0][i] == game[1][i] == game[2][i]) and (game[2][i] is not 0):
    		return game[0][i]
    return 0

def check_diag(game):
	if (game[0][0] == game[1][1] == game[2][2]) and (game[0][0] is not 0):
		return game[0][0]
	if (game[0][2] == game[1][1] == game[2][0]) and (game[0][2] is not 0):
		return game[0][2]
	return 0

def check_game(game):
	if check_rows(game) == 1 or check_diag(game) == 1 or check_cols(game) == 1:
		return 1
	elif check_rows(game) == 2 or check_diag(game) == 2 or check_cols(game) == 2:
		return 2
	return 0

print(check_game(winner_is_2))
print(check_game(winner_is_1))
print(check_game(winner_is_also_1))
print(check_game(no_winner))
print(check_game(also_no_winner))